# Issues and Issue Templates

This repository contains several examples of issue templates and how issues can
be used in an RDM context.


## Issues
In broad terms, issues in GitLab and similar software
can be used to manage tasks between members of a team. What exactly these tasks
are depends on the specific use case.

In software development, these are used to track bugs, draft new features, or
discuss the governance of an OS project.
Other possible applications are tracking consultations of clients and employing
tags to allow filtering them. However, they could also be used to tracks samples,
document data collection, organize work on a paper, or manage lab devices.

Issue texts are written in markdown. As an additional feature, issues can be
cross-referenced by typing `#` in the issue text followed be the issue number
(or the name if the issue, which is auto-completed).

Comments in the issue text can be written in HTML comment format:
```markdown
This text can be seen in the issue.
<!-- This text can only be seen when writing. -->

<!---
This is a multi line comment
only seen during writing.
--->
```

You can also attach files to an issue by dragging them into the issue text.


## Open and Closed Issues
When an issue is created, it is in the "open" state.
This means, that it will show up in the issues list and denote work that needs doing.
Once an issue is resolved, it can be closed to vanish from that list.

In the examples above, issues could be closed after fixing the described bug,
once a sample is processed completely, or after a consultation was finished.
The closed issue will remain as a documentation of that process.

There is, however, also a list of closed and of all open as well as closed issues.
If it should be required, a closed issue can also be reopened.


## Issue Templates
Issue templates are prepared texts that can be used when creating a new issue.
Their main uses are to provide standardized structures for certain issue types,
assist users in creating new issues, and prevent excessive typing.

Templates are stored as markdown files in the git repository of the project.
GitLab searches for them in the folder
```
.gitlab/issue_templates/my_template.md
```
and displays them in the "Choose a Template" drop down menu when creating a
new issue. You can see an example of that in this repository.


## Labels and Milestones
Labels and milestones are two ways to structure issues for visualization, 
filtering, and sorting.

An issue can be tagged with multiples labels, e.g. for specific topics
(`"Sample"`, `"Bug"`, `"Physics"`, `"ELN"`, ...) for required actions (`"waiting"`,
`"urgent"`, ...) or project-specific labels.
When searching for issues you can include or exclude certain labels,
e.g. show all issues labeled `"urgent"` but exclude those that are also
labeled with `"ELN"`.

Milestones are used to work towards a specific goal, usually a release
in software development. An issue can only be assigned to a single
milestone. Within that milestone, issues are grouped like a
[Kanban board](https://en.wikipedia.org/wiki/Kanban), i.e. unassigned,
assigned, and completed issues.

<!-- ## Examples -->

<!-- ... -->
