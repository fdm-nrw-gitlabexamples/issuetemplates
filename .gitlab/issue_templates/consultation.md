# Consultation Documentation
<!---
Please enter the following information in the table below.

    Date of request:          To know when the client contacted us
    Date of consultation:     When did we meet with the client
    Responsible users:        Name and user account (link this with @name)
    Contact:                  Who is the client and how do we
                              reach them? Mail, etc.
    Continuation of a
    previous consultation:    If this consultation is related to a previous
                              consultation. Consider linking that issue to
                              this one as well.

                              Enter here ↘
--->
|                                             |                                 |
| ------------------------------------------- | ------------------------------- |
| **Date of request**                         | *TODO*  |
| **Date of consultation**                    | *TODO*  |
| **Responsible users**                       | *TODO*  |
| **Contact**                                 | *TODO*  | 
| **Continuation of previous consultation?**  | no      |

## Consultation Topic
### Issues/ Goals
*TODO: What is the topic of this consultation? What does the client want?*

* TODO: Additional links and information, e.g.:*
- *Link to project website, dataset, etc. that is*
- *Links to related consultations*
- *...*

### Steps to take

*TODO: What steps do we need to take to solve this?*
- [ ] *First step*
- [ ] *Second step*
- ...

### Comments
*E.g.:*
- *What solution would be the best one?*
- **
- *Is this consultation completed?*

----

## Consultation log
<!--- ### 2021-MM-DD (Type of consultation) --->

<!--- ### 2021-MM-DD (Type of consultation) --->

<!--- ### 2021-MM-DD (Type of consultation) --->
